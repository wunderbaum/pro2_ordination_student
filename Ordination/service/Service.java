package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Service {
	private Storage storage;
	private static Service service;

	private Service() {
		storage = new Storage();
	}

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	public static Service getTestService() {
		return new Service();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke
	 *
	 * @return opretter og returnerer en PN ordination.
	 */
	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Startdatoen skal være efter den valgte slutdato");
		}
		if (antal < 1) {
			throw new IllegalArgumentException("Du skal angive minimum 1 enhed");
		}
		PN PNo = new PN(startDen, slutDen, patient, laegemiddel, antal);
		patient.addOrdination(PNo);

		return PNo;
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		if (startDen.isAfter(slutDen)) {
			throw new IllegalArgumentException("FEJL: Slutdatoen skal være efter den angivne startdato");
		}
		DagligFast DagligF = new DagligFast(startDen, slutDen, patient, laegemiddel);
		Dosis dosis1 = new Dosis(LocalTime.of(8, 00), morgenAntal);
		Dosis dosis2 = new Dosis(LocalTime.of(12, 00), middagAntal);
		Dosis dosis3 = new Dosis(LocalTime.of(18, 00), aftenAntal);
		Dosis dosis4 = new Dosis(LocalTime.of(23, 59), natAntal);
		Dosis[] doser = new Dosis[4];
		doser[0] = dosis1;
		doser[1] = dosis2;
		doser[2] = dosis3;
		doser[3] = dosis4;
		DagligF.setDoser(doser);
		patient.addOrdination(DagligF);
		laegemiddel.addOrdination(DagligF);
		return DagligF;
	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {
		if (startDen.isAfter(slutDen)) {
			throw new IllegalArgumentException("FEJL: Slutdatoen skal være efter den angivne startdato");
		}
		DagligSkaev DagligS = new DagligSkaev(startDen, slutDen, patient, laegemiddel);
		for (int i = 0; i < klokkeSlet.length; i++) {
			DagligS.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		patient.addOrdination(DagligS);
		laegemiddel.addOrdination(DagligS);
		return DagligS;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis
	 * datoen ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException
	 */
	public void ordinationPNAnvendt(PN ordination, LocalDate dato) {
		if ((dato.isBefore(ordination.getSlutDen()) || ordination.getSlutDen().isEqual(dato)) &
				(dato.isAfter(ordination.getStartDen()) || ordination.getStartDen().isEqual(dato))) {
			ordination.givDosis(dato);
		}else{
			throw new IllegalArgumentException("Dato for ordination er ugyldig");
		}
		
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn
	 * til patientens vægt). Det er en forskellig enheds faktor der skal
	 * anvendes, og den er afhængig af patientens vægt.
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double result;
		if (patient.getVaegt() < 25) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() > 120) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		} else {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer.
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {
		int AOrd = 0;
		for (Patient AOpatienter : getAllPatienter()) {
			if (AOpatienter.getVaegt() >= vægtStart && AOpatienter.getVaegt() <= vægtSlut) {
				for (int i = 0; i < AOpatienter.getOrdinationer().size(); i++) {
					if (AOpatienter.getOrdinationer().get(i).getLaegemiddel() == laegemiddel) {
						AOrd++;
					}
				}
			}
		}
		return AOrd;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en
	 * slutDato.
	 *
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	public boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		boolean result = true;
		if (slutDato.compareTo(startDato) < 0) {
			result = false;
		}
		return result;
	}

	public Patient opretPatient(String navn, String cpr, double vaegt) {
		Patient p = new Patient(navn, cpr, vaegt);
		storage.addPatient(p);
		return p;
	}

	public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
				enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public void createSomeObjects() {
		opretPatient("Jane Jensen", "121256-0512", 63.4);
		opretPatient("Finn Madsen", "070985-1153", 83.2);
		opretPatient("Hans Jørgensen", "050972-1233", 89.4);
		opretPatient("Ulla Nielsen", "011064-1522", 59.9);
		opretPatient("Ib Hansen", "090149-2529", 87.7);

		opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

}
