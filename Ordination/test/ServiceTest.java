package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {

	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.opretPatient("0701920000", "Andreas Elkjaer", 60);
		laegemiddel = new Laegemiddel("DSG", 0.05, 0.040, 0.035, "mg");
	}

	@Test
	public void testAnbefaletDosisPrDag() {
		patient.setVaegt(24);
		assertEquals(1.2, service.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(25);
		assertEquals(1, service.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(120);
		assertEquals(4.8, service.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
		patient.setVaegt(121);
		assertEquals(4.235, service.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}
	
	@Test
	public void testAntalOrdinationerPrVægtPrLaegemiddel(){
		service.opretDagligFastOrdination(LocalDate.of(2016, 9, 9), LocalDate.of(2016, 9, 12), patient, laegemiddel, 2, 1, 2, 1);
		service.opretPNOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 9, 1), patient, laegemiddel, 5);
		assertEquals(2, service.antalOrdinationerPrVægtPrLægemiddel(25, 120, laegemiddel));
		
		Patient patient2 = service.opretPatient("04071219902387", "Jesper Jensen", 125);
		LocalTime[] klokkeSlet = {LocalTime.of(12, 00), LocalTime.of(9, 00), LocalTime.of(17, 00), LocalTime.of(20, 00), LocalTime.of(16, 00)};
		double[] antalEnheder = {2, 3, 4, 2, 1};
		service.opretDagligSkaevOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 9, 9), patient2, laegemiddel, klokkeSlet, antalEnheder);
		assertEquals(2, service.antalOrdinationerPrVægtPrLægemiddel(25, 120, laegemiddel));
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(121, 150, laegemiddel));
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(0, 59, laegemiddel));
		
		Laegemiddel leagemiddel2 = service.opretLaegemiddel("Morfin", 0.02, 0.015, 0.01, "ml");
		service.opretPNOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 8, 10), patient2, leagemiddel2, 2);
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(121, 150, laegemiddel));
	}
	
	@Test
	public void testCheckStartFoerSlut(){
		assertEquals(true, service.checkStartFoerSlut(LocalDate.of(2016, 02, 02), LocalDate.of(2016, 02, 03)));
		assertEquals(false, service.checkStartFoerSlut(LocalDate.of(2016, 02, 02), LocalDate.of(2016, 02, 01)));
	}
	
	@Test
	public void testOpretPatient(){
		assertThat(service.opretPatient("04071219902387", "Jesper Jensen", 125),
				is(instanceOf(Patient.class)));
	}
	
	@Test
	public void testOpretLaegemiddel(){
		assertThat(service.opretLaegemiddel("Metformin", 50, 75, 100, "mg"),
				is(instanceOf(Laegemiddel.class)));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOrdinationPNAnvendtException(){
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 9, 1), patient, laegemiddel, 5);
		service.ordinationPNAnvendt(pn, LocalDate.of(2016, 9, 3));
	}
	
	@Test
	public void testOrdinationPNAnvendt(){
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 9, 1), patient, laegemiddel, 5);
		service.ordinationPNAnvendt(pn, LocalDate.of(2016, 8, 20));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNDatoException(){
		service.opretPNOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 8, 1), patient, laegemiddel, 5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNEnhedException(){
		service.opretPNOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 8, 10), patient, laegemiddel, 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastException(){
		service.opretDagligFastOrdination(LocalDate.of(2016, 9, 9), LocalDate.of(2016, 9, 1), patient, laegemiddel, 2, 1, 2, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevException(){
		LocalTime[] klokkeSlet = {LocalTime.of(12, 00), LocalTime.of(9, 00), LocalTime.of(17, 00), LocalTime.of(20, 00), LocalTime.of(16, 00)};
		double[] antalEnheder = {2, 3, 4, 2, 1};
		service.opretDagligSkaevOrdination(LocalDate.of(2016, 8, 9), LocalDate.of(2016, 8, 8), patient, laegemiddel, klokkeSlet, antalEnheder);
	}

}
