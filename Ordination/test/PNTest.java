package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class PNTest {
	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;
	private PN pn;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.opretPatient("0701920000", "Andreas Elkjaer", 60);
		laegemiddel = new Laegemiddel("DSG", 0.05, 0.040, 0.035, "mg");
		pn = new PN(LocalDate.of(2016, 02, 02),LocalDate.of(2016, 02, 14), patient, laegemiddel, 5);
	}
	
	@Test
	public void testGivDosis(){
		assertEquals(true, pn.givDosis(LocalDate.of(2016, 02, 02)));
		assertEquals(true, pn.givDosis(LocalDate.of(2016, 02, 14)));
		assertEquals(false, pn.givDosis(LocalDate.of(2016, 02, 15)));
		assertEquals(false, pn.givDosis(LocalDate.of(2016, 02, 01)));
	}
	
	@Test
	public void DosisTest(){
		pn.givDosis(LocalDate.of(2016, 02, 02));
		pn.givDosis(LocalDate.of(2016, 02, 05));
		pn.givDosis(LocalDate.of(2016, 02, 07));
		pn.givDosis(LocalDate.of(2016, 02, 11));
		assertEquals(2, pn.doegnDosis(), 0.01);
		assertEquals(20, pn.samletDosis(), 0.01);
		assertEquals(4, pn.getAntalGangeGivet(), 0.01);
		assertEquals(5, pn.getAntalEnheder(), 0.01);
		assertEquals("PN", pn.getType());
	}
	

}
