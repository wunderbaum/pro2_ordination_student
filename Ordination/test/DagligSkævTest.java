package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class DagligSkævTest {

	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;
	private DagligSkaev dagligSkaev; 


	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.opretPatient("0701920000", "Andreas Elkjaer", 60);
		laegemiddel = new Laegemiddel("DSG", 0.05, 0.040, 0.035, "mg");
		LocalTime[] klokkeSlet = {LocalTime.of(8, 00), LocalTime.of(12, 00), LocalTime.of(16, 00), LocalTime.of(20, 00)};
		double[] antalEnheder = {2, 1, 2 , 1};
		dagligSkaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 02, 02), LocalDate.of(2016, 02, 14),
				patient, laegemiddel, klokkeSlet, antalEnheder);
	}
	
	@Test
	public void samletDosisTest(){
		assertEquals(78, dagligSkaev.samletDosis(), 0.001);
	}
	
	@Test
	public void doegnDosisTest(){
		assertEquals(6, dagligSkaev.doegnDosis(), 0.001);
	}
	
	@Test
	public void getTypeTest(){
		assertEquals("Daglig Skæv", dagligSkaev.getType());
	}
}
