package test;

import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class BlackBoxTest {

	public static void main(String[] args) {
		Service service = Service.getTestService();
		Patient patient = service.opretPatient("0701920000", "Andreas Elkjaer", 0);
		Laegemiddel laegemiddel = null;
		service.createSomeObjects();
		for (Laegemiddel l : service.getAllLaegemidler()) {
			if (l.getNavn().equals("Paracetamol")) {
				laegemiddel = l;
			}
		}
		System.out.println(service.anbefaletDosisPrDoegn(patient, laegemiddel));
		//patient.setVaegt(-1);
		//System.out.println(service.anbefaletDosisPrDoegn(patient, laegemiddel));
		patient.setVaegt(24);
		System.out.println(service.anbefaletDosisPrDoegn(patient, laegemiddel));
		patient.setVaegt(25);
		System.out.println(service.anbefaletDosisPrDoegn(patient, laegemiddel));
		patient.setVaegt(120);
		System.out.println(service.anbefaletDosisPrDoegn(patient, laegemiddel));
		patient.setVaegt(121);
		System.out.println(service.anbefaletDosisPrDoegn(patient, laegemiddel));
	}
}
