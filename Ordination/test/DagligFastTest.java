package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class DagligFastTest {
	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;
	private DagligFast dagligFast;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.opretPatient("0701920000", "Andreas Elkjaer", 60);
		laegemiddel = new Laegemiddel("DSG", 0.05, 0.040, 0.035, "mg");
		dagligFast = service.opretDagligFastOrdination(LocalDate.of(2016, 02, 02), LocalDate.of(2016, 02, 14),
				patient, laegemiddel, 2, 1, 2, 1);
	}
	
	@Test
	public void samletDosisTest(){
		assertEquals(78, dagligFast.samletDosis(), 0.001);
	}
	
	@Test
	public void doegnDosisTest(){
		assertEquals(6, dagligFast.doegnDosis(), 0.001);
	}
	
	@Test
	public void getTypeTest(){
		assertEquals("Daglig Fast", dagligFast.getType());
	}
}
