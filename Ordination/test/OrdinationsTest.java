package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class OrdinationsTest {
	
	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.opretPatient("0701920000", "Andreas Elkjaer", 60);
		laegemiddel = new Laegemiddel("DSG", 0.05, 0.040, 0.035, "mg");
	}
	
	@Test
	public void TestPnOrdinationsMetoder() {
		PN pn = new PN(LocalDate.of(2016, 02, 02),LocalDate.of(2016, 02, 14), patient, laegemiddel, 5);
		
		assertEquals(13, pn.antalDage(), 0.001);
		assertEquals("2016-02-02", pn.toString());
		assertEquals(LocalDate.of(2016, 02, 14), pn.getSlutDen());
	}
	
	@Test
	public void TestDagligSkaevOrdinationsMetoder() {
		DagligSkaev skaev = new DagligSkaev(LocalDate.of(2016, 02, 02),LocalDate.of(2016, 02, 14), patient, laegemiddel);
		
		assertEquals(13, skaev.antalDage(), 0.001);
		assertEquals("2016-02-02", skaev.toString());
		assertEquals(LocalDate.of(2016, 02, 14), skaev.getSlutDen());
	}
	
	@Test
	public void TestDagligFastOrdinationsMetoder() {
		DagligFast fast = new DagligFast(LocalDate.of(2016, 02, 02),LocalDate.of(2016, 02, 14), patient, laegemiddel);
		
		assertEquals(13, fast.antalDage(), 0.001);
		assertEquals("2016-02-02", fast.toString());
		assertEquals(LocalDate.of(2016, 02, 14), fast.getSlutDen());
	}
}
