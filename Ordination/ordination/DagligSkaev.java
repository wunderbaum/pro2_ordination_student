package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosis;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);
		this.dosis = new ArrayList<>();
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		dosis.add(d);
	}

	@Override
	public double samletDosis() {
		int samlet = 0;
		for (int i = 0; i < dosis.size(); i++) {
			samlet += dosis.get(i).getAntal();
		}
		return samlet * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		double result = samletDosis() / antalDage();
		return result;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}

	// -------------------------------------------------------------------

	public void addDosis(Dosis dosis) {
		this.dosis.add(dosis);
	}

	public void removeDosis(Dosis dosis) {
		this.dosis.remove(dosis);
	}

	public ArrayList<Dosis> getDosis() {
		return new ArrayList<>(dosis);
	}
}