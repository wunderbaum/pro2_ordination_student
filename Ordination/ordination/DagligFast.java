package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination {
	Dosis[] dosis = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);

	}

	public Dosis[] getDosis() {
		return dosis;
	}

	@Override
	public double samletDosis() {
		double result = 0;
		for (int i = 0; i < dosis.length; i++) {
			result += dosis[i].getAntal();
		}
		return result * antalDage();
	}

	@Override
	public double doegnDosis() {
		double result = 0;
		for (int i = 0; i < dosis.length; i++) {
			result += dosis[i].getAntal();
		}
		return result;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public void setDoser(Dosis[] dosis) {
		// TODO Auto-generated method stub
		this.dosis = dosis;

	}

}