package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

import sun.applet.Main;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> dageGivet;

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, patient, laegemiddel);
		this.dageGivet = new ArrayList<>();
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if ((givesDen.isBefore(super.getSlutDen()) || super.getSlutDen().isEqual(givesDen)) &
				(givesDen.isAfter(super.getStartDen()) || super.getStartDen().isEqual(givesDen))) {
			dageGivet.add(givesDen);
			return true;
		}
		return false;
	}

	@Override
	public double doegnDosis() {
		Collections.sort(this.dageGivet);
		if (this.dageGivet.size() != 0) {
			return this.antalEnheder * this.dageGivet.size()
					/ (ChronoUnit.DAYS.between(this.dageGivet.get(0), this.dageGivet.get(this.dageGivet.size() - 1))
							+ 1);
		}
		return 0;
	}

	@Override
	public double samletDosis() {
		return this.antalEnheder * this.dageGivet.size();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return this.dageGivet.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}
}
