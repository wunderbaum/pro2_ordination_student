package ordination;

import java.util.ArrayList;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;

	private ArrayList<Ordination> ordinationer;

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.setVaegt(vaegt);
		this.ordinationer = new ArrayList<>();
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		if(vaegt < 0){
			throw new IllegalArgumentException("FEJL: Patient kan ikke havde negativ vægt");
		}else{
			this.vaegt = vaegt;
		}
	}

	public void addOrdination(Ordination ordination) {
		ordinationer.add(ordination);
	}

	public void removeOrdination(Ordination ordination) {
		ordinationer.remove(ordination);
	}

	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<>(ordinationer);
	}

	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

}
